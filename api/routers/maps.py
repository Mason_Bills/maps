from fastapi import APIRouter
import requests
import os
import uuid
from keys import mapbox_token

router = APIRouter()
mapbox_key = os.getenv('mapbox_token')

@router.get("/geocode/")
async def geocode(address: str):
    response = requests.get(
        f"https://api.mapbox.com/geocoding/v5/mapbox.places/{address}.json",
        params={"access_token": mapbox_token},
    )
    return response.json()

# @router.get("/search/")
# async def search(key_word: str, longitude: float, latitude: float):
#     session_token = uuid.uuid4()
#     response = requests.get(
#         f"https://api.mapbox.com/search/searchbox/v1/suggest?q={key_word}&proximity={longitude},{latitude}",
#         params={"access_token": mapbox_token,
#                 "session_token": session_token
#                 },
#     )
#     return response.json()

# @router.get("/search/")
# async def search(key_word: str, longitude: float, latitude: float):
#     session_token = uuid.uuid4()
#     response = requests.get(
#         f"https://api.mapbox.com/search/searchbox/v1/category",
#         params={
#                 "q": key_word,
#                 "proximity": f"{longitude},{latitude}",
#                 "access_token": mapbox_token,
#                 "session_token": session_token
#                 "type": "poi",
#                 },
#     )
#     data = response.json()
#     map_id = data["suggestions"][0]["mapbox_id"]
#     print(map_id)
#     return map_id

@router.get("/search/")
async def search(key_word: str, longitude: float, latitude: float):
    session_token = uuid.uuid4()
    response = requests.get(
        f"https://api.mapbox.com/geocoding/v5/mapbox.places/{key_word}.json",
        params={
                "access_token": mapbox_token,
                "types": "poi",
                "session_token": session_token,
                "proximity": f"{longitude},{latitude}",
                },
    )
    return response.json()

