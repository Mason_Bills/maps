import React, { useEffect, useState } from "react";
import Map, { Marker } from "react-map-gl";

function MapComponent() {
  const [viewport, setViewport] = useState({
    latitude: 40,
    longitude: -100,
    zoom: 10,
    width: "100vw",
    height: "100vh",
  });

  const [userInput, setUserInput] = useState({
    address: "",
  })

  const [marker, setMarker] = useState(
    {
      latitude: 40,
      longitude: -100,
    }
  );


  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        "http://localhost:8000/geocode?address=moab UT"
      );
      const data = await response.json();
      console.log(data);

      setMarker({
        latitude: data.features[0].center[1],
        longitude: data.features[0].center[0],
      })

      
        setViewport((prevViewport) => ({
          ...prevViewport,
          latitude: data.features[0].center[1],
          longitude: data.features[0].center[0],
        }));
      
    }
    fetchData();
  }, []);

  return (
    <div style={{ height: "100vh", width: "100vw" }}>
      <Map
        {...viewport}
        onMove={(evt) => setViewport(evt.viewState)}
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
      >
        <Marker longitude={marker.longitude} latitude={marker.latitude} />
      </Map>
    </div>
  );
}

export default MapComponent;
