import React, { useState } from "react";



function SearchComponent({marker, setMarker}) {
  const [searchTerm, setSearchTerm] = useState("");
  const [results, setResults] = useState([]);
  

  const handleInputChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSearch = async () => {
    try {
      const response = await fetch(
        `http://localhost:8000/search/?key_word=${searchTerm}&longitude=${marker.longitude}&latitude=${marker.latitude}`
      );
      if (response.data) {
        setResults(response.data.features);
      }
      console.log(response)
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <input
        type="text"
        value={searchTerm}
        onChange={handleInputChange}
        placeholder="Search for places..."
      />
      <button onClick={handleSearch}>Search</button>
      {results &&
        results.map((place, index) => (
          <div key={index}>
            <h3>{place.place_name}</h3>
            <p>
              Latitude: {place.center[1]}, Longitude: {place.center[0]}
              <button
                onClick={() =>
                  setMarker({
                    latitude: place.center[1],
                    longitude: place.center[0],
                  })
                }
              >
                Set Marker Here
              </button>
            </p>
          </div>
        ))}
    </div>
  );
}

export default SearchComponent;
