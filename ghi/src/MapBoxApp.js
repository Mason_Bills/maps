import React, { useState } from "react";
import MapComponent from "./maps.js";
import SearchComponent from "./search.js";

function MapboxApp() {
  const [searchTerm, setSearchTerm] = useState("");
  const [results, setResults] = useState([]);

  const handleInputChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSearch = async () => {
    try {
      const response = await fetch(
        `http://localhost:8000/search/?key_word=${searchTerm}`
      );
      if (response.data) {
        setResults(response.data.features);
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <SearchComponent
        searchTerm={searchTerm}
        onInputChange={handleInputChange}
        onSearch={handleSearch}
      />
      <MapComponent results={results} />
    </div>
  );
}

export default MapboxApp;
